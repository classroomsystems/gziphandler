# Automatic response compression for Go http.Handlers

[![GoDoc](https://godoc.org/bitbucket.org/classroomsystems/gziphandler?status.svg)](https://godoc.org/bitbucket.org/classroomsystems/gziphandler)
[![GoDoc](http://gocover.io/_badge/bitbucket.org/classroomsystems/gziphandler)](http://gocover.io/bitbucket.org/classroomsystems/gziphandler)

There are many packages that do this job,
but I haven't found another package that meets these requirements:

* Inaccurate Content-Length headers are never sent.
* Calling WriteHeader doesn't break anything.
* Gzip writers are reused and not created for each request.
* Content types are sniffed appropriately.
* Whenever possible, very small responses are not compressed.
* Range requests are disabled as appropriate.
* Accept-Encoding headers are parsed properly.
* Requests with a "Connection: Upgrade" header are not touched in any way.
	This facilitates WebSocket connectons and any other compatible protocol.
* Handlers may use the http.Flusher, http.Hijacker, and http.CloseNotifier interfaces
	if they are supported by the underlying ResponseWriter.
* Compression level may be specified.
* A whitelist of content types to compress may be specified.
* The package depends only on the Go standard library.

## Compression speed

If you want to use [Klaus Posts's optimized gzip package][klauspost],
import "bitbucket.org/classroomsystems/httpgzip/klauspost"
instead of "bitbucket.org/classroomsystems/httpgzip".
The interface is identical, but performance may be better.

[klauspost]: https://github.com/klauspost/compress/

## License

Copyright (c) 2016 Classroom Systems, LLC

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
