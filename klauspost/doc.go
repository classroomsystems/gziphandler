// Package gziphandler provides an http.Handler wrapper that compresses response data.
//
// This package is identical to "bitbucket.org/classroomsystems/gziphandler",
// except that it uses "github.com/klauspost/compress/gzip"
// in place of the standard library's "compress/gzip".
package gziphandler
