# NOTICE

This package is auto-generated from the one in the parent directory.
Only `doc.go` and `generate.go` should be edited here.
To update, run `go generate`.

[![GoDoc](https://godoc.org/bitbucket.org/classroomsystems/gziphandler/klauspost?status.svg)](https://godoc.org/bitbucket.org/classroomsystems/gziphandler/klauspost)
[![GoDoc](http://gocover.io/_badge/bitbucket.org/classroomsystems/gziphandler/klauspost)](http://gocover.io/bitbucket.org/classroomsystems/gziphandler/klauspost)
