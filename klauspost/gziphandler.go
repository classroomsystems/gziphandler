package gziphandler

import (
	"bufio"
	"bytes"
	"github.com/klauspost/compress/gzip"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"mime"
	"net"
	"net/http"
	"strconv"
	"strings"
	"sync"
)

const (
	minGzipLength = 128  // Don't bother compressing response bodies smaller than this.
	sniffLength   = 512  // Number of bytes to use for content-type sniffing.
	biggestBuffer = 2048 // Don't hold on to buffers bigger than this.
)

var errNotAHijacker = errors.New("The underlying ResponseWriter is not an http.Hijacker")

// These constants are copied from the gzip package, so that code that imports gziphandler
// does not also have to import "github.com/klauspost/compress/gzip".
const (
	NoCompression      = gzip.NoCompression
	BestSpeed          = gzip.BestSpeed
	BestCompression    = gzip.BestCompression
	DefaultCompression = gzip.DefaultCompression
)

// DefaultContentTypes lists the response content types that will be compressed by default.
// If the list is changed, subsequent calls to New and NewLevel will use the new defaults.
// Handlers already created are unaffected by changes here.
// See (*Handler).LimitContentTypes.
var DefaultContentTypes = []string{
	"application/javascript",
	"application/json",
	"application/x-javascript",
	"application/x-tar",
	"image/svg+xml",
	"text/css",
	"text/csv",
	"text/html",
	"text/plain",
	"text/xml",
}

// Handler is an http.Handler that gzips responses written by an underlying http.Handler.
type Handler struct {
	h     http.Handler
	types map[string]bool
	level int
	pool  *sync.Pool
}

// New returns a new Handler to gzip responses from h with DefaultCompression level.
// Initially, only content types listed in DefaultContentTypes will be compressed.
func New(h http.Handler) *Handler {
	// With DefaultCompression, no error is possible.
	nh, _ := NewLevel(h, DefaultCompression)
	return nh
}

// NewLevel returns a new Handler to gzip responses from h with the given compression level.
// Initially, only content types listed in DefaultContentTypes will be compressed.
// The returned error will be nil, unless level is an invalid compression level.
func NewLevel(h http.Handler, level int) (*Handler, error) {
	pool, err := getGzPool(level)
	if err != nil {
		return nil, err
	}
	hh := &Handler{
		h:     h,
		level: level,
		pool:  pool,
	}
	hh.LimitContentTypes(DefaultContentTypes)
	return hh, nil
}

// LimitContentTypes sets the list of content types that may be compressed.
// If types is nil, all content is compressed.
// Otherwise, only those MIME types listed will be considered for compression.
// This method must not be called if the handler may be in use by another goroutine.
func (h *Handler) LimitContentTypes(types []string) {
	if types == nil {
		h.types = nil
		return
	}
	h.types = make(map[string]bool, len(types))
	for _, t := range types {
		h.types[t] = true
	}
}

// ServeHTTP implements the http.Hander interface.
func (h *Handler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	if r.Header.Get("Connection") == "Upgrade" {
		// Don't mess with Upgrade requests, e.g. WebSockets.
		h.h.ServeHTTP(w, r)
		return
	}
	switch acceptEncoding(r) {
	case acceptNothing:
		http.Error(w, "No acceptable encoding available", http.StatusNotAcceptable)
	case acceptIdentity:
		h.h.ServeHTTP(w, r)
	case acceptGzip:
		r.Header.Del("Range")
		rw, gw := newWriter(h, w)
		h.h.ServeHTTP(rw, r)
		if !gw.hijacked {
			gw.flush(true)
			if gw.gz != nil {
				h.putGzip(gw.gz)
			}
		}
	}
}

// shouldGzip returns true if a response with the given header values should be compressed.
func (h *Handler) shouldGzip(head http.Header) bool {
	if head.Get("Content-Encoding") != "" {
		// Don't mess with a content encoding set elsewhere.
		return false
	}
	if cl := head.Get("Content-Length"); cl != "" {
		n, err := strconv.Atoi(cl)
		if err == nil && n < minGzipLength {
			// Don't compress really short responses.
			return false
		}
	}
	if h.types == nil {
		return true
	}
	ct, _, err := mime.ParseMediaType(head.Get("Content-Type"))
	if err != nil {
		return false
	}
	return h.types[ct]
}

func (h *Handler) getGzip(w io.Writer) *gzip.Writer {
	if i := h.pool.Get(); i != nil {
		gz := i.(*gzip.Writer)
		gz.Reset(w)
		return gz
	}
	// No possible error, since we've verified h.level is good.
	gz, _ := gzip.NewWriterLevel(w, h.level)
	return gz
}

func (h *Handler) putGzip(gz *gzip.Writer) {
	h.pool.Put(gz)
}

type writer struct {
	h        *Handler
	w        http.ResponseWriter
	status   int
	hijacked bool
	out      io.Writer
	buf      *bytes.Buffer
	gz       *gzip.Writer
}

var _ http.ResponseWriter = &writer{}
var _ http.Flusher = &writer{}

// newWriter returns both an http.ResponseWriter and the concrete *writer
// so that the caller doesn't have to care whether we're just using a writer
// or wrapping one in closeNotifyWriter.
func newWriter(h *Handler, w http.ResponseWriter) (http.ResponseWriter, *writer) {
	if _, ok := w.(http.CloseNotifier); ok {
		rw := &closeNotifyWriter{writer{h: h, w: w, status: 200}}
		return rw, &rw.writer
	}
	rw := &writer{h: h, w: w, status: 200}
	return rw, rw
}

func (w *writer) Header() http.Header {
	return w.w.Header()
}

func (w *writer) Write(b []byte) (int, error) {
	if w.out != nil {
		return w.out.Write(b)
	}
	if w.buf == nil {
		w.buf = getBuf()
	}
	n, _ := w.buf.Write(b)
	if w.buf.Len() < sniffLength {
		return n, nil
	}
	err := w.flush(false)
	return n, err
}

func (w *writer) WriteHeader(status int) {
	// Don't actually write anything until the first flush.
	// We can make better compression decisions that way.
	w.status = status
}

func (w *writer) handleHeader(allDone bool) bool {
	head := w.Header()
	if w.buf != nil {
		if allDone && head.Get("Content-Length") == "" {
			head.Set("Content-Length", fmt.Sprint(w.buf.Len()))
		}
		if head.Get("Content-Type") == "" {
			head.Set("Content-Type", http.DetectContentType(w.buf.Bytes()))
		}
	}
	shouldGzip := w.h.shouldGzip(head)
	if shouldGzip {
		head.Set("Content-Encoding", "gzip")
		head.Del("Content-Length")
	}
	head.Add("Vary", "Accept-Encoding")
	head.Del("Accept-Ranges")
	w.w.WriteHeader(w.status)
	return shouldGzip
}

func (w *writer) Flush() {
	w.flush(false)
}

func (w *writer) flush(allDone bool) error {
	var err error
	if w.out == nil {
		if shouldGzip := w.handleHeader(allDone); shouldGzip {
			w.gz = w.h.getGzip(w.w)
			w.out = w.gz
		} else {
			w.out = w.w
		}
		if w.buf != nil {
			_, err = w.out.Write(w.buf.Bytes())
			putBuf(w.buf)
			w.buf = nil
		}
	}
	if err == nil && w.gz != nil {
		err = w.gz.Flush()
	}
	if f, ok := w.w.(http.Flusher); ok {
		f.Flush()
	}
	return err
}

func (w *writer) Hijack() (net.Conn, *bufio.ReadWriter, error) {
	if hj, ok := w.w.(http.Hijacker); ok {
		w.hijacked = true
		return hj.Hijack()
	}
	return nil, nil, errNotAHijacker
}

// closeNotifyWriter is a separate type so that
// our ResponseWriters only implement CloseNotify
// when the underlying writer does.
type closeNotifyWriter struct {
	writer
}

func (w *closeNotifyWriter) CloseNotify() <-chan bool {
	// We established that w.w is a CloseNotifier in newWriter.
	return w.w.(http.CloseNotifier).CloseNotify()
}

var bufPool = new(sync.Pool)

func getBuf() *bytes.Buffer {
	if b := bufPool.Get(); b != nil {
		return b.(*bytes.Buffer)
	}
	return new(bytes.Buffer)
}

func putBuf(b *bytes.Buffer) {
	b.Reset()
	if b.Cap() <= biggestBuffer {
		bufPool.Put(b)
	}
}

var gzPools struct {
	sync.Mutex
	levels map[int]*sync.Pool
}

func init() {
	gzPools.levels = make(map[int]*sync.Pool)
}

func getGzPool(level int) (*sync.Pool, error) {
	gzPools.Lock()
	defer gzPools.Unlock()
	p := gzPools.levels[level]
	if p == nil {
		_, err := gzip.NewWriterLevel(ioutil.Discard, level)
		if err != nil {
			return nil, err
		}
		p = new(sync.Pool)
		gzPools.levels[level] = p
	}
	return p, nil
}

type acceptResult int

const (
	acceptNothing acceptResult = iota
	acceptIdentity
	acceptGzip
)

func acceptEncoding(r *http.Request) acceptResult {
	s := r.Header.Get("Accept-Encoding")
	identityQ := -1.0
	gzipQ := -1.0
	starQ := -1.0
	for _, enc := range strings.Split(s, ",") {
		nm, q := parseEncoding(enc)
		switch nm {
		case "identity":
			identityQ = q
		case "gzip", "x-gzip":
			gzipQ = q
		case "*":
			starQ = q
		}
	}
	if gzipQ > 0 && gzipQ >= identityQ {
		return acceptGzip
	}
	if identityQ == 0 || identityQ < 0 && starQ == 0 {
		return acceptNothing
	}
	return acceptIdentity
}

func parseEncoding(enc string) (name string, q float64) {
	s := strings.SplitN(enc, ";", 2)
	name = strings.ToLower(strings.TrimSpace(s[0]))
	if len(s) == 1 {
		return name, 1
	}
	// Malformed q parameters will be treated as q=1.
	s[1] = strings.TrimSpace(s[1])
	if s[1][0] != 'q' {
		return name, 1
	}
	s[1] = strings.TrimSpace(s[1][1:])
	if s[1][0] != '=' {
		return name, 1
	}
	q, err := strconv.ParseFloat(strings.TrimSpace(s[1][1:]), 64)
	if err != nil {
		return name, 1
	}
	if q < 0 {
		q = 0
	}
	if q > 1 {
		q = 1
	}
	return name, q
}
