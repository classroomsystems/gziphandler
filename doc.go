// Package gziphandler provides an http.Handler wrapper that compresses response data.
package gziphandler
