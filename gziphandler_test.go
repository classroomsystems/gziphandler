package gziphandler

import (
	"bufio"
	"bytes"
	"compress/gzip"
	"errors"
	"fmt"
	"io"
	"net"
	"net/http"
	"net/http/httptest"
	"testing"
)

// TestGzip tests that content is properly compressed or not.
func TestGzip(t *testing.T) {
	empty := []byte{}
	short := []byte("This is short, but not-empty, text content.\n")
	notCompressed := []struct {
		content   []byte
		fn        http.HandlerFunc
		unlimited bool
	}{
		{
			content: empty,
			fn:      func(w http.ResponseWriter, r *http.Request) {},
		},
		{
			content: empty,
			fn: func(w http.ResponseWriter, r *http.Request) {
				w.Write(empty)
			},
		},
		{
			content: empty,
			fn: func(w http.ResponseWriter, r *http.Request) {
				w.Header().Set("Content-Length", "0")
				w.Write(empty)
			},
		},
		{
			content: empty,
			fn: func(w http.ResponseWriter, r *http.Request) {
				w.Header().Set("Content-Length", "0")
				w.WriteHeader(200)
			},
		},
		{
			content: empty,
			fn: func(w http.ResponseWriter, r *http.Request) {
				w.Header().Set("Content-Length", "0")
				w.(http.Flusher).Flush()
			},
		},
		{
			content: short,
			fn: func(w http.ResponseWriter, r *http.Request) {
				w.Write(short)
			},
		},
		{
			content: short,
			fn: func(w http.ResponseWriter, r *http.Request) {
				w.Header().Set("Content-Length", fmt.Sprint(len(short)))
				w.Write(short)
			},
		},
		{
			content:   short,
			unlimited: true,
			fn: func(w http.ResponseWriter, r *http.Request) {
				w.Header().Set("Content-Length", fmt.Sprint(len(short)))
				w.Header().Set("Content-Type", "application/octet-stream")
				w.WriteHeader(200)
				w.Write(short)
			},
		},
		{
			content: bytes.Repeat(short, 10),
			fn: func(w http.ResponseWriter, r *http.Request) {
				w.Header().Set("Content-Length", fmt.Sprint(len(short)*10))
				w.Header().Set("Content-Type", "application/octet-stream")
				w.WriteHeader(200)
				for i := 0; i < 10; i++ {
					w.Write(short)
				}
			},
		},
		{
			content: bytes.Repeat(short, 2),
			fn: func(w http.ResponseWriter, r *http.Request) {
				w.Header().Set("Content-Length", fmt.Sprint(len(short)*2))
				w.Header().Set("Content-Type", "text/plain; charset=utf-8")
				w.WriteHeader(200)
				w.Write(short)
				w.(http.Flusher).Flush()
				w.Write(short)
			},
		},
		{
			content: bytes.Repeat(short, 10),
			fn: func(w http.ResponseWriter, r *http.Request) {
				w.Header().Set("Content-Encoding", "identity")
				w.Header().Set("Content-Length", fmt.Sprint(len(short)*10))
				w.Header().Set("Content-Type", "text/plain; charset=utf-8")
				w.WriteHeader(200)
				for i := 0; i < 10; i++ {
					w.Write(short)
					w.(http.Flusher).Flush()
				}
			},
		},
	}
	for i, tc := range notCompressed {
		var w *httptest.ResponseRecorder
		if tc.unlimited {
			w = testFuncUnlimited(tc.fn)
		} else {
			w = testFunc(tc.fn)
		}
		if cl := w.HeaderMap.Get("Content-Length"); cl != "" && cl != fmt.Sprint(w.Body.Len()) {
			t.Errorf("%d: bad Content-Length: %s != %d", i, cl, w.Body.Len())
		}
		if w.Body.Len() != len(tc.content) {
			t.Errorf("%d: bad body length: %d != %d", i, w.Body.Len(), len(tc.content))
		}
		if !bytes.Equal(w.Body.Bytes(), tc.content) {
			t.Errorf("%d: bad content: %q != %q", i, w.Body.Bytes(), tc.content)
		}
		if w.HeaderMap.Get("Content-Encoding") == "gzip" {
			t.Errorf("%d: compressed short response", i)
		}
	}

	compressed := []struct {
		content   []byte
		fn        http.HandlerFunc
		unlimited bool
	}{
		{
			content: empty,
			fn: func(w http.ResponseWriter, r *http.Request) {
				w.Header().Set("Content-Type", "text/plain; charset=utf-8")
				w.(http.Flusher).Flush()
			},
		},
		{
			content: bytes.Repeat(short, 2),
			fn: func(w http.ResponseWriter, r *http.Request) {
				w.Header().Set("Content-Type", "text/plain; charset=utf-8")
				w.WriteHeader(200)
				w.Write(short)
				w.(http.Flusher).Flush()
				w.Write(short)
			},
		},
		{
			content: bytes.Repeat(short, 10),
			fn: func(w http.ResponseWriter, r *http.Request) {
				w.Header().Set("Content-Length", fmt.Sprint(len(short)*10))
				w.Header().Set("Content-Type", "text/plain; charset=utf-8")
				w.WriteHeader(200)
				for i := 0; i < 10; i++ {
					w.Write(short)
				}
			},
		},
		{
			content: bytes.Repeat(short, 10),
			fn: func(w http.ResponseWriter, r *http.Request) {
				w.Header().Set("Content-Type", "text/plain; charset=utf-8")
				w.WriteHeader(200)
				for i := 0; i < 10; i++ {
					w.Write(short)
				}
			},
		},
		{
			content: bytes.Repeat(short, 10),
			fn: func(w http.ResponseWriter, r *http.Request) {
				w.Header().Set("Content-Length", fmt.Sprint(len(short)*10))
				w.Header().Set("Content-Type", "text/plain; charset=utf-8")
				w.WriteHeader(200)
				for i := 0; i < 10; i++ {
					w.Write(short)
					w.(http.Flusher).Flush()
				}
			},
		},
		{
			content: bytes.Repeat(short, 10),
			fn: func(w http.ResponseWriter, r *http.Request) {
				w.Header().Set("Content-Type", "text/plain; charset=utf-8")
				w.WriteHeader(200)
				for i := 0; i < 10; i++ {
					w.Write(short)
					w.(http.Flusher).Flush()
				}
			},
		},
		{
			content:   bytes.Repeat(short, 10),
			unlimited: true,
			fn: func(w http.ResponseWriter, r *http.Request) {
				w.Header().Set("Content-Length", fmt.Sprint(len(short)*10))
				w.Header().Set("Content-Type", "application/octet-stream")
				w.WriteHeader(200)
				for i := 0; i < 10; i++ {
					w.Write(short)
				}
			},
		},
	}
	for i, tc := range compressed {
		var w *httptest.ResponseRecorder
		if tc.unlimited {
			w = testFuncUnlimited(tc.fn)
		} else {
			w = testFunc(tc.fn)
		}
		if cl := w.HeaderMap.Get("Content-Length"); cl != "" && cl != fmt.Sprint(w.Body.Len()) {
			t.Errorf("%d: bad Content-Length: %s != %d", i, cl, w.Body.Len())
		}
		if w.HeaderMap.Get("Content-Encoding") != "gzip" {
			t.Errorf("%d: missing Content-Encoding: gzip", i)
		}
		gz, err := gzip.NewReader(w.Body)
		if err != nil {
			t.Errorf("%d: gzip error: %v", i, err)
			continue
		}
		buf := make([]byte, 2048)
		n, err := io.ReadFull(gz, buf)
		if err != nil && err != io.EOF && err != io.ErrUnexpectedEOF {
			t.Errorf("%d: gzip read error: %v", i, err)
			continue
		}
		buf = buf[:n]
		if len(buf) != len(tc.content) {
			t.Errorf("%d: bad body length: %d != %d", i, len(buf), len(tc.content))
		}
		if !bytes.Equal(buf, tc.content) {
			t.Errorf("%d: bad content: %q != %q", i, buf, tc.content)
		}
	}
}

// TestContentType tests that Content-Type detection plays well with compression.
func TestContentType(t *testing.T) {
	tcs := []struct {
		contentType     string
		contentEncoding string
		data            []byte
	}{
		{"text/plain; charset=utf-8", "", []byte("The quick brown fox jumps over the lazy dog.\n")},
		{"text/plain; charset=utf-8", "gzip", bytes.Repeat([]byte("The quick brown fox jumps over the lazy dog.\n"), 1000)},
		{"text/html; charset=utf-8", "", []byte("<!DOCTYPE html><p>Some text.</p>")},
		{"text/html; charset=utf-8", "gzip", bytes.Repeat([]byte("<!DOCTYPE html><p>Some text.</p>"), 1000)},
	}
	for i, tc := range tcs {
		w := testFunc(func(w http.ResponseWriter, r *http.Request) {
			for i := range tc.data {
				// write a byte at a time to test buffering
				w.Write(tc.data[i : i+1])
			}
		})
		if ct := w.HeaderMap.Get("Content-Type"); ct != tc.contentType {
			t.Errorf("%d: Bad Content-Type: %s", i, ct)
		}
		if ce := w.HeaderMap.Get("Content-Encoding"); ce != tc.contentEncoding {
			t.Errorf("%d: Unexpected Content-Encoding: %q != %q", i, ce, tc.contentEncoding)
		}
	}
}

// TestRange tests that the Range and Accept-Ranges headers are removed when requesting gzip.
func TestRange(t *testing.T) {
	hadRange := false
	makeRequest := func(doGzip bool) *httptest.ResponseRecorder {
		var fn http.HandlerFunc = func(w http.ResponseWriter, r *http.Request) {
			if r.Header.Get("Range") != "" {
				hadRange = true
			}
			w.Header().Set("Accept-Ranges", "bytes")
			w.WriteHeader(200)
		}
		r, err := http.NewRequest("GET", "http://example.com", nil)
		if err != nil {
			panic(err) // This should be impossible
		}
		if doGzip {
			r.Header.Set("Accept-Encoding", "gzip")
		}
		r.Header.Set("Range", "bytes=0-5")
		w := httptest.NewRecorder()
		h := New(fn)
		h.ServeHTTP(w, r)
		return w
	}
	w := makeRequest(false)
	if !hadRange {
		t.Error("Range header wrongly stripped")
	}
	if w.HeaderMap.Get("Accept-Ranges") == "" {
		t.Error("Accept-Ranges header wrongly stripped")
	}

	hadRange = false
	w = makeRequest(true)
	if hadRange {
		t.Error("Range header not stripped")
	}
	if w.HeaderMap.Get("Accept-Ranges") != "" {
		t.Error("Accept-Ranges header not stripped")
	}
}

// TestUnacceptable tests that the handler returns http.StatusNotAcceptable when necessary.
func TestUnacceptable(t *testing.T) {
	var fn http.HandlerFunc = func(w http.ResponseWriter, r *http.Request) {}
	r, err := http.NewRequest("GET", "http://example.com", nil)
	if err != nil {
		panic(err) // This should be impossible
	}
	r.Header.Set("Accept-Encoding", "compress,*;q=0")
	w := httptest.NewRecorder()
	h := New(fn)
	h.ServeHTTP(w, r)
	if w.Code != http.StatusNotAcceptable {
		t.Error("accepted unacceptable request")
	}
}

// TestUpgrade tests that HTTP Upgrade requests are not messed with.
// This is mostly for WebSocket support, but any other upgrade protocol should work as well.
func TestUpgrade(t *testing.T) {
	makeRequest := func(doUpgrade bool) *httptest.ResponseRecorder {
		var fn http.HandlerFunc = func(w http.ResponseWriter, r *http.Request) {
			w.Header().Set("Content-Type", "text/plain")
			w.WriteHeader(200)
			w.Write(bytes.Repeat([]byte("abcdefgh"), 1024/8))
		}
		r, err := http.NewRequest("GET", "http://example.com", nil)
		if err != nil {
			panic(err) // This should be impossible
		}
		r.Header.Set("Accept-Encoding", "gzip")
		if doUpgrade {
			r.Header.Set("Connection", "Upgrade")
			r.Header.Set("Upgrade", "SomeOtherProtocol")
		}
		w := httptest.NewRecorder()
		h := New(fn)
		h.ServeHTTP(w, r)
		return w
	}
	w := makeRequest(false)
	if w.HeaderMap.Get("Vary") != "Accept-Encoding" {
		t.Error("Vary header not set")
	}
	if w.HeaderMap.Get("Content-Encoding") != "gzip" {
		t.Error("Response not compressed")
	}

	w = makeRequest(true)
	if w.HeaderMap.Get("Vary") == "Accept-Encoding" {
		t.Error("Vary header set")
	}
	if w.HeaderMap.Get("Content-Encoding") == "gzip" {
		t.Error("Response compressed")
	}
}

// TestInterfaces tests that our ResponseWriters implement
// http.Flusher, http.Hijacker, and http.CloseNotifier.
func TestInterfaces(t *testing.T) {
	var isFlusher, isHijacker, isCloseNotifier bool
	var hijackErr error
	var ch <-chan bool
	makeRequest := func(w http.ResponseWriter) {
		var fn http.HandlerFunc = func(w http.ResponseWriter, r *http.Request) {
			if f, ok := w.(http.Flusher); ok {
				isFlusher = true
				f.Flush()
			}
			if h, ok := w.(http.Hijacker); ok {
				isHijacker = true
				_, _, hijackErr = h.Hijack()
			}
			if c, ok := w.(http.CloseNotifier); ok {
				isCloseNotifier = true
				ch = c.CloseNotify()
			}
		}
		r, err := http.NewRequest("GET", "http://example.com", nil)
		if err != nil {
			panic(err) // This should be impossible
		}
		r.Header.Set("Accept-Encoding", "gzip")
		h := New(fn)
		h.ServeHTTP(w, r)
	}

	makeRequest(dummyWriter{})
	if !isFlusher {
		t.Error("Not a Flusher")
	}
	if !isHijacker {
		t.Error("Not a Hijacker")
	}
	if hijackErr != errNotAHijacker {
		t.Error("Unexpected hijack error:", hijackErr)
	}
	if isCloseNotifier {
		t.Error("Wrongly a CloseNotifier")
	}

	bigDummy := &bigDummyWriter{ch: make(chan bool, 0)}
	makeRequest(bigDummy)
	if !isFlusher {
		t.Error("Not a Flusher")
	}
	if !bigDummy.flushCalled {
		t.Error("Flush not called")
	}
	if !isHijacker {
		t.Error("Not a Hijacker")
	}
	if hijackErr != errHijackSuccess {
		t.Error("Unexpected hijack error:", hijackErr)
	}
	if !isCloseNotifier {
		t.Error("Not a CloseNotifier")
	}
	if ch != bigDummy.ch {
		t.Errorf("Wrong CloseNotify channel: %#v", ch)
	}
}

func testFunc(fn http.HandlerFunc) *httptest.ResponseRecorder {
	r, err := http.NewRequest("GET", "http://example.com", nil)
	if err != nil {
		panic(err) // This should be impossible
	}
	r.Header.Set("Accept-Encoding", "gzip")
	w := httptest.NewRecorder()
	h := New(fn)
	h.ServeHTTP(w, r)
	return w
}

func testFuncUnlimited(fn http.HandlerFunc) *httptest.ResponseRecorder {
	r, err := http.NewRequest("GET", "http://example.com", nil)
	if err != nil {
		panic(err) // This should be impossible
	}
	r.Header.Set("Accept-Encoding", "gzip")
	w := httptest.NewRecorder()
	h := New(fn)
	h.LimitContentTypes(nil)
	h.ServeHTTP(w, r)
	return w
}

func TestAcceptEncoding(t *testing.T) {
	tcs := []struct {
		value    string
		expected acceptResult
	}{
		{"", acceptIdentity},
		{"compress, gzip", acceptGzip},
		{"compress,*;q=0", acceptNothing},
		{"compress;q=1,x-GZIP;q=0.7;identity;q=0", acceptGzip},
		{"gzip ;q=0", acceptIdentity},
		{"gzip", acceptGzip},
		{"gzip;q=0.5,identity;q=1", acceptIdentity},
		{"identity; q = 1, gzip;q=0.999", acceptIdentity},
		{"identity;q=0", acceptNothing},
		{"x-gzip", acceptGzip},
		{"identity,gzip", acceptGzip},
		{"identity;q=0.5,gzip;q=0.5", acceptGzip},

		// Test some bad ones for fuller test coverage.
		{"gzip;q=1,x-gzip;q=0", acceptIdentity},
		{"gzip;q=0,x-gzip;q=1", acceptGzip},
		{",", acceptIdentity},
		{",*=0", acceptIdentity},
		{"gzip;HUH?", acceptGzip},
		{"gzip;q+0,identity;q+1", acceptGzip},
		{"gzip;q=-5,identity;q=100", acceptIdentity},
		{"gzip;q=caesar,identity;q=salad", acceptGzip},
	}
	for _, tc := range tcs {
		r, err := http.NewRequest("GET", "http://example.com", nil)
		if err != nil {
			panic(err) // This should be impossible
		}
		r.Header.Set("Accept-Encoding", tc.value)
		actual := acceptEncoding(r)
		if actual != tc.expected {
			t.Errorf("%q: %d != %d", tc.value, actual, tc.expected)
		}
	}
}

func TestBadCompression(t *testing.T) {
	var h http.HandlerFunc = func(w http.ResponseWriter, r *http.Request) {}
	_, err := NewLevel(h, -100)
	if err == nil {
		t.Error("NewLevel didn't error with bad compression")
	}
}

type dummyWriter struct{}

var _ http.ResponseWriter = dummyWriter{}

func (w dummyWriter) Header() http.Header         { return http.Header{} }
func (w dummyWriter) Write(b []byte) (int, error) { return len(b), nil }
func (w dummyWriter) WriteHeader(int)             {}

type bigDummyWriter struct {
	ch          <-chan bool
	flushCalled bool
}

var _ http.ResponseWriter = &bigDummyWriter{}
var _ http.CloseNotifier = &bigDummyWriter{}
var _ http.Flusher = &bigDummyWriter{}
var _ http.Hijacker = &bigDummyWriter{}

func (w *bigDummyWriter) Header() http.Header         { return http.Header{} }
func (w *bigDummyWriter) Write(b []byte) (int, error) { return len(b), nil }
func (w *bigDummyWriter) WriteHeader(int)             {}
func (w *bigDummyWriter) CloseNotify() <-chan bool    { return w.ch }
func (w *bigDummyWriter) Flush()                      { w.flushCalled = true }
func (w *bigDummyWriter) Hijack() (net.Conn, *bufio.ReadWriter, error) {
	return nil, nil, errHijackSuccess
}

var errHijackSuccess = errors.New("for testing: bigDummyWriter is an http.Hijacker")
